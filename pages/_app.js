import Layout from '../components/Layout'
import React from 'react'
import NextApp, { Container } from 'next/app'

export default class App extends NextApp {
  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Container>
    )
  }
}
