const withMDX = require('@zeit/next-mdx')()

module.exports = withMDX({
  pageExtensions:['ts', 'tsx', 'js', 'jsx', 'mdx'],
  target: "serverless"
})
