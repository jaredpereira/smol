import styled from 'styled-components'
export const Red = styled('span')`
color: ${props => props.theme.accentRed};
font-weight: bold;
`
export const Blue = styled('span')`
color: ${props => props.theme.primaryBlue};
font-weight: bold;
`

export const Yellow = styled('span')`
color: ${props => props.theme.accentYellow};
font-weight: bold;
`
