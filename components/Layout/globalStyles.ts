import {createGlobalStyle} from 'styled-components'
import {theme} from './theme'

export const GlobalStyles = createGlobalStyle<{theme: typeof theme}>`
body {
  margin: 64px auto;
  max-width: 536px;
  padding:0 32px;

  background-color: ${props => props.theme.white};
  color: ${props => props.theme.gray100}

  font-size: 16px;
  font-family: 'Roboto', sans-serif;
}

@font-face {
  font-display: swap;
}


a, a:visited {
  color: ${props => props.theme.gray100}
  text-decoration: underline;
}
`
