import h from 'react-hyperscript'
import {ThemeProvider} from 'styled-components'
import {SFC, Fragment} from 'react'

import Header from './Header'

import {theme} from './theme'
import {GlobalStyles} from './globalStyles'

const Layout:SFC = (props) => {
  return h(ThemeProvider, {theme}, [
    h(Fragment, [
      h(GlobalStyles),
      h(Header),
      [props.children]
    ])
  ])
}


export default Layout
