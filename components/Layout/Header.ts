import h from 'react-hyperscript'
import styled from 'styled-components'

export default () => {
  return h(Container, [
    h(Logo, {href: "/"}, "smol"),
    h(Login, {href:"/login"}, "login")
  ])
}

const Logo = styled('a')`
text-decoration: none;
z-index: 1;
font-size: 40px;
line-height:30px;
font-family: Futura;
`

const Login = styled('a')`
z-index: 1;
justify-self: right;
`

const Container = styled('div')`
display: grid;
grid-template-columns: auto auto;
`
