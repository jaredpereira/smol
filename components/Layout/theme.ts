export const theme = {
  white: "#FFFFFF",
  gray20: "#DEDEDE",
  gray40: "#B3B3B3",
  gray60: "#7A7A7A",
  gray80: "#4E4E4E",
  gray100: "#282828",
  accentRed: "#B2261D",
  primaryBlue: "#81BEE0",
  accentYellow: "#F4CC64"
}

type Theme = typeof theme
declare module "styled-components" {
  interface DefaultTheme extends Theme {}
}
