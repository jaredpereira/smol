import h from 'react-hyperscript'
import styled from 'styled-components'

export const Section = styled('div')<{border?: boolean, center?: boolean}>`
padding: 32px 0;
display: grid;
grid-template-columns: repeat(4, 1fr);
grid-column-gap: 16px;

${props => props.border ?
    `border-top: solid 1px;
     border-bottom: solid 1px;
     border-color: ${props.theme.gray60};
    `: null
}}

${props => props.center ? "text-align: center;" : null}
`

export const TextBlock = styled('div')`
grid-column: 1/-1;

ol {
  list-style: none;
  counter-reset: item;
  padding: 0;
}

li {
  counter-increment: item;
  margin: 16px 0px;
}

li:before {
  content: counter(item);
  font-weight: bold;
  display: block;
}
`

export const Big = styled('h2')`
margin: -0.5em 0px;
color: ${props => props.theme.primaryBlue};
`

export const HeaderImg = () => {
  return h(StyledImg, {src: '/static/landing-splash-compressed.png'})
}

const StyledImg = styled('img')`
padding: 0px 32px;
z-index:-1;
margin-top: -64px;
margin-bottom: -80px;
height: 100%;
width: calc(100% - 64px);
`
