import styled from 'styled-components'
import h from 'react-hyperscript'

export const Button:React.SFC<{dark:boolean, text:string, col: string, left:boolean}> = (props) => {
  return h(Container, props, [
    h(StyledButton, props, [
      h('span', [props.text]),
      h(ButtonImg, {src: props.dark ? '/static/icons/arrow-light.svg' : '/static/icons/arrow-dark.svg'})
    ]),
    props.children ? h(HelpText, [props.children]) : null
  ])
}

const Container = styled('div')<{col:string, left:boolean}>`
grid-column: ${props => props.col}
width: 160px;

${props=> props.left ? "justify-self:right;" : null}
`

const HelpText = styled('div')`
font-size: 0.75em;
text-align: center;
color: ${props => props.theme.gray60}
`

const ButtonImg = styled('img')`
padding: 0px 8px;
vertical-align: middle;
`

const StyledButton = styled('button')<{dark: boolean, col: string}>`
height: 48px;
width: 100%;
margin-bottom: 4px;

font-size: 1em;
font-weight: bold;

background-color: ${props => props.dark ? props.theme.gray100 : 'transparent'};
color: ${props => props.dark ? props.theme.white : 'inherit'};

border: solid 2px;
border-radius: 4px;
box-sizing: border-box;


:hover {
cursor: pointer;
}
`
